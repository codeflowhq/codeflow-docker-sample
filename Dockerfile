FROM codeflowlang/codeflow:0.7.9
WORKDIR /app

RUN /bin/sh -c "apk add --no-cache bash"

COPY package*.json .

RUN npm install
RUN codeflow install

COPY . .

ENV PORT=3005
EXPOSE 3005

CMD ["run", "server.flw"]
